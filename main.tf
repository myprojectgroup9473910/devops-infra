provider "aws" {
  region = "us-east-1"  # Replace with your actual region
}

# Terraform backend for remote state storage on S3
terraform {
  backend "s3" {
    bucket = "tfstatebucketforcounterservice"  
    key    = "terraform.tfstate"
  }
}

# Create VPC with at least two Availability Zones (AZs)
resource "aws_vpc" "counter_service_vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "counter-service-vpc"
  }
}

variable "availability_zones" {
  type        = list(string)
  description = "List of Availability Zones for VPC"
  default     = ["us-east-1a", "us-east-1b"]  
}

resource "aws_subnet" "public_subnet" {
  vpc_id            = aws_vpc.counter_service_vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = var.availability_zones[0]
  map_public_ip_on_launch = true  # Enable auto-assign public IP addresses

  tags = {
    Name = "counter-service-public-subnet"
  }
}

resource "aws_subnet" "private_subnet" {
  vpc_id            = aws_vpc.counter_service_vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = var.availability_zones[1]

  tags = {
    Name = "counter-service-private-subnet"
  }
}

# Internet gateway
resource "aws_internet_gateway" "counter_service_igw" {
  vpc_id = aws_vpc.counter_service_vpc.id

  tags = {
    Name = "counter-service-igw"
  }
}

# Route table (associate with public subnet)
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.counter_service_vpc.id

  tags = {
    Name = "counter-service-public-route-table"
  }
}

# Associate the route table with the public subnet
resource "aws_route_table_association" "public_association" {
  subnet_id      = aws_subnet.public_subnet.id
  route_table_id = aws_route_table.public_route_table.id
}

# Create a default route to the internet gateway
resource "aws_route" "public_route" {
  route_table_id         = aws_route_table.public_route_table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.counter_service_igw.id
}

# Security group (allow SSH, HTTP, and service ports)
resource "aws_security_group" "counter_service_sg" {
  name   = "counter-service-sg"
  vpc_id = aws_vpc.counter_service_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443  # Example port for bonus feature
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "counter-service-sg"
  }
}

# IAM Role for EKS Cluster
resource "aws_iam_role" "eks_cluster_role" {
  name               = "eks-cluster-role"
  assume_role_policy = jsonencode({
    "Version"               : "2012-10-17",
    "Statement"             : [
      {
        "Effect"    : "Allow",
        "Principal" : {
          "Service": "eks.amazonaws.com"
        },
        "Action"    : "sts:AssumeRole"
      }
    ]
  })
}

# EKS cluster
resource "aws_eks_cluster" "counter_service_cluster" {
  name = "counter-service"
  role_arn = aws_iam_role.eks_cluster_role.arn
  
  vpc_config {
    subnet_ids         = [aws_subnet.public_subnet.id, aws_subnet.private_subnet.id]
    security_group_ids = [aws_security_group.counter_service_sg.id]
  }
}

# EKS node group
resource "aws_eks_node_group" "counter_service_node_group" {
  cluster_name    = aws_eks_cluster.counter_service_cluster.name
  node_group_name = "counter-service-node-group"
  node_role_arn   = aws_iam_role.eks_node_role.arn

  subnet_ids         = [aws_subnet.public_subnet.id, aws_subnet.private_subnet.id]  # Use existing subnets
  instance_types     = ["t3.medium"]  # Choose instance type
  scaling_config {
    desired_size = 2
    max_size     = 3
    min_size     = 1
  }
}

# IAM Role for EKS Node Group
resource "aws_iam_role" "eks_node_role" {
  name = "eks-node-role"

  assume_role_policy = jsonencode({
    "Version"               : "2012-10-17",
    "Statement"             : [
      {
        "Effect"    : "Allow",
        "Principal" : {
          "Service": "ec2.amazonaws.com"
        },
        "Action"    : "sts:AssumeRole"
      }
    ]
  })
}

# IAM Policy for EKS Node Group
resource "aws_iam_policy_attachment" "eks_node_policy_attachment" {
  name       = "eks-node-policy-attachment"
  roles      = [aws_iam_role.eks_node_role.name]
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
}

# IAM Policy for EKS Node Group
resource "aws_iam_policy_attachment" "eks_cni_policy_attachment" {
  name       = "eks-cni-policy-attachment"
  roles      = [aws_iam_role.eks_node_role.name]
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
}
